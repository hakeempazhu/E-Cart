<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2><span>e</span>-cart</h2>
							<p>A New Shopping Experience</p>
						</div>
					</div>
					
					<div class="col-sm-3">
						<div class="address">
							<img src="images/home/map.png" alt="" />
							<p>Govt.Engineering College,Thrissur,Kerala  IND(India)</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2016 E-Cart Inc. All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="#">Hakeem,Vishnu,Gopika & Yamuna</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
