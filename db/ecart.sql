-- phpMyAdmin SQL Dump
-- version 2.11.2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 06, 2016 at 06:04 PM
-- Server version: 5.0.45
-- PHP Version: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `ecart`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `l_id` int(6) NOT NULL auto_increment,
  `user_name` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY  (`l_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`l_id`, `user_name`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tb_brand`
--

CREATE TABLE `tb_brand` (
  `brand_id` int(6) NOT NULL auto_increment,
  `brand_name` varchar(50) NOT NULL,
  PRIMARY KEY  (`brand_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tb_brand`
--

INSERT INTO `tb_brand` (`brand_id`, `brand_name`) VALUES
(1, 'Dell'),
(2, 'Acer'),
(3, 'Sony'),
(18, 'Nikon'),
(8, 'Samsung'),
(19, 'HTC'),
(13, 'Apple'),
(15, 'CANON');

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE `tb_category` (
  `category_id` varchar(30) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `shipping_cost` varchar(5) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `status` varchar(8) NOT NULL,
  PRIMARY KEY  (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`category_id`, `category_name`, `shipping_cost`, `Description`, `status`) VALUES
('laptops', 'Laptops', '60', 'veriety of brands and models', 'ENABLE'),
('mobilephones', 'Mobile Phones', '50', 'veriety of brands and models', 'ENABLE'),
('Mp3_player', 'Mp3 players', '35', 'good products', 'ENABLE'),
('digitalcameras', 'Digital Cameras', '40', 'Good brands and models...............', 'ENABLE'),
('smart1', 'Smart Watches', '50', 'Smart watches', 'ENABLE');

-- --------------------------------------------------------

--
-- Table structure for table `tb_currency`
--

CREATE TABLE `tb_currency` (
  `id` int(5) NOT NULL auto_increment,
  `USD` varchar(20) NOT NULL,
  `GBP` varchar(20) NOT NULL,
  `EUR` varchar(20) NOT NULL,
  `INR` varchar(20) NOT NULL,
  `default_currency` varchar(5) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_currency`
--

INSERT INTO `tb_currency` (`id`, `USD`, `GBP`, `EUR`, `INR`, `default_currency`) VALUES
(1, '0.022502', '0.0139599231', '0.0158086272', '1', 'INR');

-- --------------------------------------------------------

--
-- Table structure for table `tb_information`
--

CREATE TABLE `tb_information` (
  `id` int(3) NOT NULL auto_increment,
  `about_us` varchar(1500) NOT NULL,
  `privacy_policy` varchar(1500) NOT NULL,
  `terms_and_conditions` varchar(1500) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_information`
--

INSERT INTO `tb_information` (`id`, `about_us`, `privacy_policy`, `terms_and_conditions`) VALUES
(1, '  Our site is no.1 in india in online shopping and e-commerce. you can buy many items like electronics, vehicle....and also you can sell your product online', 'Add Privacy policy', 'add terms and conditions');

-- --------------------------------------------------------

--
-- Table structure for table `tb_model`
--

CREATE TABLE `tb_model` (
  `model_id` int(6) NOT NULL auto_increment,
  `brand_id` varchar(20) NOT NULL,
  `category_id` varchar(20) NOT NULL,
  `model_name` varchar(50) NOT NULL,
  PRIMARY KEY  (`model_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tb_model`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_order_list`
--

CREATE TABLE `tb_order_list` (
  `order_id` int(6) NOT NULL auto_increment,
  `customer_id` varchar(100) NOT NULL,
  `product_id` varchar(20) NOT NULL,
  `quantity` varchar(20) NOT NULL,
  `price` varchar(10) NOT NULL,
  `shipping_cost` varchar(10) NOT NULL,
  `date` varchar(15) NOT NULL,
  `status` varchar(10) NOT NULL,
  `shipped_date` varchar(30) NOT NULL,
  PRIMARY KEY  (`order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_order_list`
--

INSERT INTO `tb_order_list` (`order_id`, `customer_id`, `product_id`, `quantity`, `price`, `shipping_cost`, `date`, `status`, `shipped_date`) VALUES
(1, '4', 'iphone1', '1', '68000', '501', '11-11-2016', 'SHIPPED', '11/Nov/2016');

-- --------------------------------------------------------

--
-- Table structure for table `tb_product`
--

CREATE TABLE `tb_product` (
  `product_id` varchar(50) NOT NULL,
  `category_name` varchar(20) NOT NULL,
  `company_price` varchar(20) NOT NULL,
  `price` varchar(20) NOT NULL,
  `offer_price` varchar(20) NOT NULL,
  `stock` varchar(10) NOT NULL,
  `shipping_cost` varchar(5) NOT NULL,
  `description` varchar(1500) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `brand_name` varchar(50) NOT NULL,
  `status` varchar(8) NOT NULL,
  `simulator` varchar(30) default NULL,
  `video` varchar(300) default NULL,
  PRIMARY KEY  (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_product`
--

INSERT INTO `tb_product` (`product_id`, `category_name`, `company_price`, `price`, `offer_price`, `stock`, `shipping_cost`, `description`, `product_name`, `brand_name`, `status`, `simulator`, `video`) VALUES
('aspire4738z', 'Laptops', '24200', '30000', '28500', '0', '40', 'good laptop', 'Aspire 4738Z', 'Acer', 'ENABLE', NULL, NULL),
('viaop', 'Laptops', '26500', '30000', '28000', '86', '40', 'good netbook.\r\nlight weight ultra portable', 'Viao P', 'Sony', 'ENABLE', NULL, NULL),
('ipod-nano', 'Mp3 players', '10000', '14000', '11999', '98', '35', 'ipod 7th generation\r\n16gb internal\r\ngood mp3 player', 'Ipod nano', 'Apple', 'ENABLE', '', NULL),
('iphone1', 'Mobile Phones', '45000', '60000', '48999', '4', '50', 'iPhone 7\r\n128 GB \r\n3 GB RAM\r\n21 MP Camera', 'iPhone 7', 'Apple', 'ENABLE', 'ios7-demo.html', NULL),
('macbook1', 'Laptops', '75000', '120000', '110999', '10', '50', '2.0GHz dual-core Intel Core i5 processor\r\nTurbo Boost up to 3.1GHz\r\n256GB PCIe-based SSD\r\n8GB of 1866MHz memory\r\nIntel Iris Graphics 540\r\nForce Touch trackpad\r\nTwo Thunderbolt 3 ports\r\n', 'Mac Book Pro', 'Apple', 'ENABLE', 'macosx.html', 'http://www.youtube.com/embed/wdhsyiusi'),
('s7edge1', 'Mobile Phones', '45000', '48000', '47000', '2', '50', '5.5 inches (~76.1% screen-to-body ratio)\r\nResolution	1440 x 2560 pixels (~534 ppi pixel density)\r\nMultitouch	Yes\r\nProtection	Corning Gorilla Glass 4', 'Samsung Galaxy S7 Edge', 'Samsung', 'ENABLE', 'android.html', NULL),
('samsunggear', 'Smart Watches', '3600', '4000', '3800', '8', '50', 'Size	1.63 inches (~39.8% screen-to-body ratio)\r\nResolution	320 x 320 pixels (~278 ppi pixel density)\r\nMultitouch	Yes', 'Samsung Gear 2', 'Samsung', 'ENABLE', 'watch.html', NULL),
('dell_studio_15', 'Laptops', '38900', '42500', '41750', '86', '40', 'good laptop from dell', 'Dell Studio 15', 'Dell', 'ENABLE', NULL, NULL),
('Canon_G12', 'Digital Cameras', '27900', '32200', '30999', '100', '40', 'Good professional camera from Canon', 'Canon G12', 'CANON', 'ENABLE', NULL, NULL),
('CanonEOS_5D', 'Digital Cameras', '42000', '47500', '45999', '9', '50', 'Good DSLR camera from Canon', 'Canon EOS 5D', 'CANON', 'ENABLE', 'camera.swf', 'http://www.youtube.com/embed/camera'),
('ipod5', 'Mp3 players', '12000', '14000', '12999', '8', '35', 'good mp3 player', 'Ipod Touch', 'Apple', 'ENABLE', 'Name with Extension', NULL),
('ipod3', 'Mp3 players', '4000', '6000', '4999', '4', '35', '16 gb internal', 'Ipod Shuffle', 'Apple', 'ENABLE', 'Name with Extension', NULL),
('ipod4', 'Mp3 players', '8000', '9000', '8500', '3', '35', 'good mp3 player', 'Ipod Classic', 'Apple', 'ENABLE', 'Name with Extension', NULL),
('camera3', 'Digital Cameras', '38000', '45000', '40999', '6', '40', 'good camera', 'Nikon D300', 'Nikon', 'ENABLE', 'Name with Extension', NULL),
('iwatch1', 'Smart Watches', '19000', '22000', '21999', '8', '50', '128 internal\r\n2gb ram', 'I Watch', 'Apple', 'ENABLE', 'Name with Extension', NULL),
('samsngtab1', 'Mobile Phones', '18000', '21000', '19999', '10', '50', '16 gb internal \r\n2gb ram\r\n1080p display', 'Samsung Tab', 'Samsung', 'ENABLE', 'Name with Extension', NULL),
('htc1', 'Mobile Phones', '10000', '15000', '13999', '3', '50', 'good phone', 'HTC Touch', 'HTC', 'ENABLE', 'Name with Extension', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_review`
--

CREATE TABLE `tb_review` (
  `id` int(10) NOT NULL auto_increment,
  `product_id` varchar(50) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `review` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_review`
--

INSERT INTO `tb_review` (`id`, `product_id`, `name`, `email`, `review`) VALUES
(1, 'viaop', 'dsfsfd', 'sas@ds.com', 'sdfdfsdfsvxcvxc');

-- --------------------------------------------------------

--
-- Table structure for table `tb_shoppingcart`
--

CREATE TABLE `tb_shoppingcart` (
  `cart_id` int(10) NOT NULL auto_increment,
  `customer_id` varchar(20) NOT NULL,
  `product_id` varchar(20) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `quantity` varchar(20) NOT NULL,
  `price` varchar(20) NOT NULL,
  `shipping_cost` varchar(5) NOT NULL,
  PRIMARY KEY  (`cart_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `tb_shoppingcart`
--

INSERT INTO `tb_shoppingcart` (`cart_id`, `customer_id`, `product_id`, `product_name`, `quantity`, `price`, `shipping_cost`) VALUES
(44, '2712403', 'samsunggear', 'Samsung Gear 2', '1', '3800', '50'),
(43, '2712403', 's7edge1', 'Samsung Galaxy S7 Edge', '1', '47000', '50'),
(47, '1353150', 'dell_studio_15', 'Dell Studio 15', '1', '41750', '40'),
(48, '1508179', 'macbook1', 'Mac Book Pro', '1', '72000', '60'),
(49, '3471375', 'viaop', 'Viao P', '1', '28000', '40'),
(50, '3805542', 'viaop', 'Viao P', '1', '28000', '40'),
(51, '6057435', 'dell_studio_15', 'Dell Studio 15', '1', '41750', '40'),
(55, '5847779', 'viaop', 'Viao P', '1', '28000', '40'),
(56, '9566041', 'viaop', 'Viao P', '1', '28000', '40'),
(57, '5031128', 'macbook1', 'Mac Book Pro', '1', '110999', '50');

-- --------------------------------------------------------

--
-- Table structure for table `tb_size`
--

CREATE TABLE `tb_size` (
  `s_id` int(6) NOT NULL auto_increment,
  `product_id` varchar(20) NOT NULL,
  `s_size` varchar(20) NOT NULL,
  `quantity` varchar(20) NOT NULL,
  PRIMARY KEY  (`s_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tb_size`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `customer_id` int(6) NOT NULL auto_increment,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `company` varchar(30) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `city` varchar(30) NOT NULL,
  `postcode` varchar(30) NOT NULL,
  `countrey` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY  (`customer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`customer_id`, `first_name`, `last_name`, `email`, `phone_number`, `company`, `address1`, `address2`, `city`, `postcode`, `countrey`, `state`, `password`, `status`) VALUES
(4, 'Vishnu', 'PK', 'vishnu466@gmail.com', '9999999999', 'MATRIX', 'asdsdgsg', 'sdfsdsdfds POST', 'Kozhikode', '673014', 'India', 'Kerala', 'vishnu466', 'ACTIVE'),
(6, 'Hakeem', 'Ali', 'hakeem@gmail.com', '8888888888', 'sdadas', 'adasasd', 'asdsadsd POST', 'Kodungallur', '666666', 'India', 'KERALA', 'hakeem', 'ACTIVE'),
(64, 'Yamuna', 'Ravi', 'yamuna@gmail.com', '7777777777', 'aaaaaaa', 'sdfdsgdgh', 'dfgfdghdfgfd', 'fgdgfd', '768798', 'IN', 'kerala', 'yamuna', 'ACTIVE'),
(65, 'Gopika', 'Mohan', 'gopika@gmail.com', '9898989898', 'assdds', 'gyjhjh', 'uhhiji', 'hhjjhj', '567894', 'IN', 'kerala', 'gopika', 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `tb_viewed`
--

CREATE TABLE `tb_viewed` (
  `id` int(15) NOT NULL auto_increment,
  `product_id` varchar(100) NOT NULL,
  `count` varchar(5) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `tb_viewed`
--

INSERT INTO `tb_viewed` (`id`, `product_id`, `count`) VALUES
(28, 'dell_studio_15', '19'),
(25, 's7edge1', '8'),
(26, 'aspire4738z', '39'),
(27, 'viaop', '17'),
(24, 'samsunggear', '11'),
(23, 'iphone1', '30'),
(29, 'macbook1', '34'),
(30, 'ipod-nano', '2'),
(32, 'ipod5', '1'),
(33, 'CanonEOS_5D', '1'),
(34, 'ipod4', '1'),
(35, 'iwatch1', '3'),
(36, 'samsngtab1', '3');
