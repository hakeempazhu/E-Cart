<?php
session_start();
  if(isset($_SESSION['customer_id']))
  {
$customer_id=$_SESSION['customer_id'];
}
if(isset($_GET['error']))
{
$error=$_GET['error'];
}
else
{
$error="";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="images/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
</head>
<body>

    <?php include('header.php');?>
 
<div>
<div align="center" >
<div class="main_container">
  <table width="1024px" class="theme_color">
    <tr>
      <td width="200" valign="top"><br />
        <div style="padding-bottom:15px; width:200px;">
          <div class="theme_color2" align="center">BRANDS</div>
          <div style="border:#000099 1px dotted;"><br />
            <div align="center">
              <form action="select_brand.php" method="post">
                <select style="width:150px;" name="brand" id="brand" >
                  <option value="selectbrand">Select Brand</option>
                  <?php include('connection.php'); $result=mysql_query("SELECT * FROM tb_brand");
	while($row=mysql_fetch_array($result))
	{?>
                  <option value=<?php echo $row['brand_name'];?>><?php echo $row['brand_name'];?></option>
                  <?php
	}
	?>
                  </option>
                </select>
                <br />
                <input type="submit" value="Go" />
              </form>
              <br />
            </div>
          </div>
        </div>
        <br />
        <div style="padding-bottom:15px; width:200px;">
          <div class="theme_color2" align="center">INFORMATION</div>
          <div style="border:#000099 1px dotted;">
            <div align="left">
              <ul>
                <li>About Us</li>
                <li>Privacy policy</li>
                <li>Terms & conditions</li>
                <li>Contact us</li>
                <li>Sitemap</li>
              </ul>
            </div>
          </div>
        </div></td>
      <td style="padding-left:15px; padding-right:15px;" align="center" valign="top"><br />
        <table cellpadding="10px" cellspacing="10px">
          <tr>
            <td width="275px" style="border:1px dotted #000033;" valign="top"><div align="left">Registered user</div>
              <form action="logincheck.php" method="post">
                <table align="center" width="275px;" cellpadding="5px" cellspacing="5px">
                  <tr>
                    <td colspan="2" align="center" style="color:#000000;" class="theme_color2" valign="middle">::  Login ::</td>
                  </tr>
                  <tr>
                    <td style="padding-left:15px;">Email</td>
                    <td align="center"><input type="text" name="username" style="width:150px; border:#000099 1px solid;" /></td>
                  </tr>
                  <tr>
                    <td style="padding-left:15px;">Password</td>
                    <td align="center"><input type="password" name="password" style="width:150px; border:#000099 1px solid;" /></td>
                  </tr>
                  <tr><td colspan="2" align="center"><font color="#FF0000"><?php echo $error;?></font></td></tr>
                  <tr>                    <td style="padding-left:15px;" colspan="2"><a href="forgot_password.php">Forgot my password</a></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="center"><input type="submit" value="Login" class="btn btn-default get" /></td>
                  </tr>
                </table>
              </form></td>
            <td width="300px" valign="top" style="border:1px dotted #000033;"><div align="left">I'm a new customer</div>
              <br />
              <p align="justify">By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made</p>
              <br />
              <div align="center"><a href="register.php" style="width:35px; height:20px;">Register</a></div></td>
          </tr>
        </table></td>
      <td width="200" valign="top"><br>
        <br>
        <div >
        <div align="center" class="theme_color2" style="width:200px;">SHOPPING CART</div>
        <?php
  if(isset($_SESSION['customer_id']))
  {
$customer_id=$_SESSION['customer_id'];
$res=mysql_query("SELECT * FROM tb_shoppingcart where customer_id='$customer_id'");
echo '<table width="200px;"  style="border:#000099 1px dotted;">';
while($row=mysql_fetch_array($res))
	{
			echo '<tr><td align="left">'. $row['quantity'].'</td><td align="left"> x ';?>
        <a href=product.php?product_id=<?php echo $row['product_id'];?>><?php echo $row['product_name'].'</a></td><td align="left">'.round(($row['quantity']*$row['price'])*$_SESSION['rate'],2).'</td></tr>';
}
?>
        <tr>
            <td colspan="3"><div align="center"><a href="clear_cart.php">Clear my cart </a></div></td>
          </tr>
          <?php
}
else
{
echo '<div align="center" style="border:#000099 1px dotted; width:198px;">No Items</div>';
}
?>
      </td>
    </tr>
  </table>
</div>
</td>
</tr>
</table>
<!--End of main container-->

    <?php include('footer.php');?>

</body>
</html>
