<?php
session_start();
  if(isset($_SESSION['customer_id']))
  {
$customer_id=$_SESSION['customer_id'];
}
if(!(isset($_SESSION['user'])))
{
header( "Location: login.php" );
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="images/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Account</title>
<script type="text/javascript" language="javascript">
function showUser(str)
{
if (str=="")
  {
  document.getElementById("txtHint").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","account_order_status.php?q="+str,true);
xmlhttp.send();
}
</script>

</head>
<body onload="showUser('5')">
    <?php include('header.php');?>
 <div>
<div align="center" >
<div class="main_container" style="width:1024px;">
  <table width="1024px" class="theme_color">
    <tr>
      <td width="200" valign="top"><br />
        <div style="padding-bottom:15px; width:200px;">
          <div class="theme_color2" align="center">BRANDS</div>
          <div style="border:#000099 1px dotted;"><br />
            <div align="center">
              <form action="select_brand.php" method="post">
                <select style="width:150px;" name="brand" id="brand" >
                  <option value="selectbrand">Select Brand</option>
                  <?php include('connection.php'); $result=mysql_query("SELECT * FROM tb_brand");
	while($row=mysql_fetch_array($result))
	{?>
                  <option value=<?php echo $row['brand_name'];?>><?php echo $row['brand_name'];?></option>
                  <?php
	}
	?>
                  </option>
                </select>
                <br />
                <input type="submit" value="Go" />
              </form>
              <br />
            </div>
          </div>
        </div>
        <br />
        <div style="padding-bottom:15px; width:200px;">
          <div class="theme_color2" align="center">INFORMATION</div>
          <div style="border:#000099 1px dotted;">
            <div align="left">
              <ul>
                <li>About Us</li>
                <li>Privacy policy</li>
                <li>Terms & conditions</li>
                <li>Contact us</li>
                <li>Sitemap</li>
              </ul>
            </div>
          </div>
        </div></td>
      <td style="padding-left:15px; padding-right:15px;" align="center" valign="top"><br />
        <br />
        <div align="left" style="padding-left:15px; line-height:30px;"><a href="change_password.php">Change my password</a><br />
          <a href="change_account_details.php">Edit my Personal details</a><br />
        View my order status</div><br /><hr/>
        <div><b>Your recent order status</b>&nbsp;&nbsp;&nbsp;<select name="limit" onchange="showUser(this.value)"><option value="All">All</option><option value="5" selected="selected">Last 5</option><option value="10">Last 10</option></select><br /><br /><div id="txtHint"></div></div>
              <br />
<br />
</td>
      <td width="200" valign="top"><br>
        <br>
        <div >
        <div align="center" class="theme_color2" style="width:200px;">SHOPPING CART</div>
        <?php
  if(isset($_SESSION['customer_id']))
  {
$customer_id=$_SESSION['customer_id'];
$res=mysql_query("SELECT * FROM tb_shoppingcart where customer_id='$customer_id'");
echo '<table width="200px;"  style="border:#000099 1px dotted;">';
while($row=mysql_fetch_array($res))
	{
			echo '<tr><td align="left">'. $row['quantity'].'</td><td align="left"> x ';?>
        <a href=product.php?product_id=<?php echo $row['product_id'];?>><?php echo $row['product_name'].'</a></td><td align="left">'.round(($row['quantity']*$row['price'])*$_SESSION['rate'],2).'</td></tr>';
}
?>
        <tr>
            <td colspan="3"><div align="center"><a href="clear_cart.php">Clear my cart </a></div></td>
          </tr>
          <?php
}
else
{
echo '<div align="center" style="border:#000099 1px dotted; width:198px;">No Items</div>';
}
?>
      </td>
    </tr>
  </table>
</div>
</td>
</tr>
</table>
<!--End of main container-->
    <?php include('footer.php');?>
</body>
</html>
