<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>rEGISTER USER</title>
<link rel="stylesheet" href="images/style.css" />
<script type="text/javascript" language="javascript">
function showAvailability(str)
{
if (str=="")
  {
  document.getElementById("txtHint").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","availability.php?q="+str,true);
xmlhttp.send();
}

function validate()
{
var count=1;
var errors='';
var first_name=document.getElementById('first_name').value;
var last_name=document.getElementById('last_name').value;
var email=document.getElementById('email').value;
var reg2 = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
var phone_number=document.getElementById('phone_number').value;
var company=document.getElementById('company').value;
var address1=document.getElementById('address1').value;
var address2=document.getElementById('address2').value;
var city=document.getElementById('city').value;
var postcode=document.getElementById('postcode').value;
var countrey=document.getElementById('countrey').value;
var state=document.getElementById('state').value;
var password=document.getElementById('password').value;
var confirm_password=document.getElementById('confirm_password').value;

if(first_name=="")
{
document.reg.first_name.className="input_style2";
errors+=count+'_'+'Please enter First Name.\n';
count++;}
else
{
	document.reg.first_name.className="input_style";
	}

if(last_name=="")
{
errors+=count+'_'+'Please enter Last Name.\n';
document.reg.last_name.className="input_style2";
count++;
}
else
{
	document.reg.last_name.className="input_style";
	}

if(email=="")
{
errors+=count+'_'+'Please enter your email address.\n';
document.reg.email.className="input_style2";
count++;}
else if(reg2.test(email) == false) {
errors+=count+'_'+'Please enter a valid email address.\n';
document.reg.email.className="input_style2";
       }
	   else
{
	document.reg.email.className="input_style";
	}
if(phone_number=="")
	{
		errors+=count+'_'+'Please enter Phone Number.\n';
		document.reg.phone_number.className="input_style2";
		count++;
	}	   
		else if(isNaN(phone_number))
		{
			errors+=count+'_'+'phone number must be numerical.\n';
			document.reg.phone_number.className="input_style2";
		}
		else{document.reg.phone_number.className="input_style";};
	
if(company=="")
{
errors+=count+'- '+name+' please enter your company.\n';
document.reg.company.className="input_style2";
count++;
}
else
{
	document.reg.company.className="input_style";
	}
if(address1=="")
{
errors+=count+'_'+'Please enter your address.\n';
document.reg.address1.className="input_style2";
count++;}
else
{
	document.reg.address1.className="input_style";
	}
if(address2=="")
{
errors+=count+'_'+'Please enter your address.\n';
document.reg.address2.className="input_style2";
count++;}
else
{
	document.reg.address2.className="input_style";
	}
if(city=="")
{
errors+=count+'_'+'Please enter your city.\n';
document.reg.city.className="input_style2";
count++;}
else
{
	document.reg.city.className="input_style";
	}
if(postcode=="")
{
errors+=count+'_'+'Please enter your postcode.\n';
document.reg.postcode.className="input_style2";
count++;}
else if(isNaN(postcode))
		{
errors+=count+'_'+'Postcode must be numerical.\n';
document.reg.postcode.className="input_style2";
		}
else{
document.reg.postcode.className="input_style";}
if(countrey=="select")
{
errors+=count+'_'+'Please select your countrey.\n';
document.reg.countrey.className="input_style2";
count++;}
else
{
	document.reg.countrey.className="input_style";
	}
if(state==""){
errors+=count+'_'+'Please enter Region or State.\n';
document.reg.state.className="input_style2";
count++;}
else
{
	document.reg.state.className="input_style";
	}

if(password=="")
{
document.reg.password.className="input_style2";
errors+=count+'_'+'Enter password.\n';
}
else if(password.length<7)
{
document.reg.password.className="input_style2";
errors+=count+'_'+'password must be 6 charectors.\n';
}
else
{
document.reg.password.className="input_style";
}

if(confirm_password=="")
{
document.reg.confirm_password.className="input_style2";
errors+=count+'_'+'Enter confirm password.\n';
}
else if(password!=confirm_password)
{
document.reg.password.className="input_style2";
errors+=count+'_'+'Password missmatch.\n';
document.reg.confirm_password.className="input_style2";
count++;}
else
{
document.reg.password.className="input_style";
document.reg.confirm_password.className="input_style";
}
if(errors)
{
alert('The following '+count+' error(s) occurred:\n'+errors);
document.getElementById('error_msg').innerHTML="<font color=red>Please check red highlighted fields</font>";
document.returnValue = (errors == '');
}
else
{
document.reg.action.reset();
}
}

</script>
</head>
<body>

    <?php
include('header.php');
?>
 
<div style="width:100%;" align="center">
  <div id="main_container">
    <table width="1024px" border="0" align="center" class="theme_color">
      <tr>
        <td width="200" valign="top"><br />
          <div style="padding-bottom:15px; width:200px;">
            <div class="theme_color2" align="center">BRANDS</div>
            <div style="border:#000099 1px dotted;"><br />
              <div align="center">
                <form action="select_brand.php" method="post">
                  <select style="width:150px;" name="brand" id="brand" >
                    <option value="selectbrand">Select Brand</option>
                    <?php include('connection.php'); $result=mysql_query("SELECT * FROM tb_brand");
	while($row=mysql_fetch_array($result))
	{?>
                    <option value=<?php echo $row['brand_name'];?>><?php echo $row['brand_name'];?></option>
                  
                    <?php
	}
	?>
                    </option>
                  </select>
                  <br />
                  <input type="submit" value="Go" />
                </form>
                <br />
              </div>
            </div>
          </div>
          <br />
          <div style="padding-bottom:15px; width:200px;">
            <div class="theme_color2" align="center">INFORMATION</div>
            <div style="border:#000099 1px dotted;">
              <div align="left">
                <ul>
                  <li>About Us</li>
                  <li>Privacy policy</li>
                  <li>Terms & conditions</li>
                  <li>Contact us</li>
                  <li>Sitemap</li>
                </ul>
              </div>
            </div>
          </div></td>
        <td style="padding-left:15px; padding-right:15px;" align="center" valign="top"><br />
          <table width="550px">
            <form action="register_user.php" name="reg" method="post" onsubmit="validate();return document.returnValue">
              <tr><tr><td colspan="2" align="center"><div id="error_msg"></div></td></tr>
                <td><div align="left"><b>Your personal details</b></div>
                  <table  style="border:1px #000066 dotted; padding-left:15px;" width="100%">
                    <tr>
                      <td width="34%" align="left" style="widows:200px;">* First name</td>
                      <td width="66%" align="left"><input name="first_name" id="first_name" type="text" class="input_style" /></td>
                    </tr>
                    <tr>
                      <td align="left">* Last name</td>
                      <td align="left"><input name="last_name" id="last_name" type="text" class="input_style" /></td>
                    </tr>
                    <tr>
                      <td align="left">* Email</td>
                      <td align="left"><input name="email" id="email" type="text" class="input_style" onkeyup="showAvailability(this.value)"/><span id="txtHint"></span></td>
                    </tr>
                    <tr>
                      <td align="left">* Phone number</td>
                      <td align="left"><input name="phone_number" id="phone_number" type="text" class="input_style" /></td>
                    </tr>
                  </table>
                  <br />
                  <div align="left"><b>Your address</b></div>
                  <table width="550px" style="border:1px #000066 dotted; padding-left:15px;">
                    <tr>
                      <td width="180" align="left">Company</td>
                      <td width="359" align="left"><input name="company" id="company" type="text" class="input_style" /></td>
                    </tr>
                    <tr>
                      <td align="left">* Address1</td>
                      <td align="left"><input name="address1" id="address1" type="text" class="input_style" /></td>
                    </tr>
                    <tr>
                      <td align="left">* Address2</td>
                      <td align="left"><input name="address2" id="address2" type="text" class="input_style" /></td>
                    </tr>
                    <tr>
                      <td align="left">* City</td>
                      <td align="left"><input name="city" id="city" type="text" class="input_style" /></td>
                    </tr>
                    <tr>
                      <td align="left">* Postcode</td>
                      <td align="left"><input name="postcode" id="postcode" type="text" class="input_style" /></td>
                    </tr>
                    <tr>
                      <td align="left">* Country</td>
                      <td align="left"><select name="countrey" id="countrey" class="input_style">
                      <option value="select" selected="selected">Select</option>
                           <option value="United States">United States</option>
				<option value="AF">Afghanistan </option>
				<option value="AX">Aland Islands</option> 
  <option value="AL">Albania </option> 
  <option value="DZ">Algeria </option> 
  <option value="AS">American Samoa</option> 
  <option value="AD">Andorra</option> 
  <option value="AO">Angola</option> 
  <option value="AI">Anguilla</option> 
  <option value="AQ">Antarctica </option>      
  <option value="AG"  > Antigua and Barbuda</option>   
  <option value="AR">Argentina</option>
   <option value="AM" > Armenia  </option>        
    <option value="AW" >Aruba </option>        
    <option value="AU"
          
          > 
  Australia  </option> 
  <option value="AT"
          
          > 
  Austria  </option> 
  <option value="AZ"
          
          > 
  Azerbaijan  </option> 
  <option value="BS"
          
          > 
  Bahamas  </option> 
  <option value="BH"
          
          > 
  Bahrain  </option> 
  <option value="BD"
          
          > 
  Bangladesh  </option> 
  <option value="BB"
          
          > 
  Barbados  </option> 
  <option value="BY"
          
          > 
  Belarus  </option> 
  <option value="BE"
          
          > 
  Belgium  </option> 
  <option value="BZ"
          
          > 
  Belize  </option> 
  <option value="BJ"
          
          > 
  Benin  </option> 
  <option value="BM"
          
          > 
  Bermuda  </option> 
  <option value="BT"
          
          > 
  Bhutan  </option> 
  <option value="BO"
          
          > 
  Bolivia  </option> 
  <option value="BA"
          
          > 
  Bosnia  </option> 
  <option value="BW"
          
          > 
  Botswana  </option> 
  <option value="BV"
          
          > 
  Bouvet Island  </option> 
  <option value="BR"
          
          > 
  Brazil  </option> 
  <option value="IO"
          
          > 
  British Indian Ocean Territory  </option> 
  <option value="BN"
          
          > 
  Brunei  </option> 
  <option value="BG"
          
          > 
  Bulgaria  </option> 
  <option value="BF"
          
          > 
  Burkina Faso  </option> 
  <option value="BI"
          
          > 
  Burundi  </option> 
  <option value="KH"
          
          > 
  Cambodia  </option> 
  <option value="CM"
          
          > 
  Cameroon  </option> 
  <option value="CA"
          
          > 
  Canada  </option> 
  <option value="CV"
          
          > 
  Cape Verde  </option> 
  <option value="KY"
          
          > 
  Cayman Islands  </option> 
  <option value="CF"
          
          > 
  Central African Republic  </option> 
  <option value="TD"
          
          > 
  Chad  </option> 
  <option value="CL"
          
          > 
  Chile  </option> 
  <option value="CN"
          
          > 
  China  </option> 
  <option value="CX"
          
          > 
  Christmas Island  </option> 
  <option value="CC"
          
          > 
  Cocos Islands  </option> 
  <option value="CO"
          
          > 
  Colombia  </option> 
  <option value="KM"
          
          > 
  Comoros  </option> 
  <option value="CG"
          
          > 
  Congo  </option> 
  <option value="CD"
          
          > 
  Congo,  </option> 
  <option value="CK"
          
          > 
  Cook Islands  </option> 
  <option value="CR"
          
          > 
  Costa Rica  </option> 
  <option value="CI"
          
          > 
  Côte  </option> 
  <option value="HR"
          
          > 
  Croatia  </option> 
  <option value="CU"
          
          > 
  Cuba  </option> 
  <option value="CY"
          
          > 
  Cyprus  </option> 
  <option value="CZ"
          
          > 
  Czech Republic  </option> 
  <option value="DK"
          
          > 
  Denmark  </option> 
  <option value="DJ"
          
          > 
  Djibouti  </option> 
  <option value="DM"
          
          > 
  Dominica  </option> 
  <option value="DO"
          
          > 
  Dominican Republic  </option> 
  <option value="EC"
          
          > 
  Ecuador  </option> 
  <option value="EG"
          
          > 
  Egypt  </option> 
  <option value="SV"
          
          > 
  El Salvador  </option> 
  <option value="GQ"
          
          > 
  Equatorial Guinea  </option> 
  <option value="ER"
          
          > 
  Eritrea  </option> 
  <option value="EE"
          
          > 
  Estonia  </option> 
  <option value="ET"
          
          > 
  Ethiopia  </option> 
  <option value="FK"
          
          > 
  Falkland Islands  </option> 
  <option value="FO"
          
          > 
  Faroe Islands  </option> 
  <option value="FJ"
          
          > 
  Fiji  </option> 
  <option value="FI"
          
          > 
  Finland  </option> 
  <option value="FR"
          
          > 
  France  </option> 
  <option value="GF"
          
          > 
  French Guiana  </option> 
  <option value="PF"
          
          > 
  French Polynesia  </option> 
  <option value="TF"
          
          > 
  French Southern  </option> 
  <option value="GA"
          
          > 
  Gabon  </option> 
  <option value="GM"
          
          > 
  Gambia  </option> 
  <option value="GE"
          
          > 
  Georgia  </option> 
  <option value="DE"
          
          > 
  Germany  </option> 
  <option value="GH"
          
          > 
  Ghana  </option> 
  <option value="GI"
          
          > 
  Gibraltar  </option> 
  <option value="GR"
          
          > 
  Greece  </option> 
  <option value="GL"
          
          > 
  Greenland  </option> 
  <option value="GD"
          
          > 
  Grenada  </option> 
  <option value="GP"
          
          > 
  Guadeloupe  </option> 
  <option value="GU"
          
          > 
  Guam  </option> 
  <option value="GT"
          
          > 
  Guatemala  </option> 
  <option value="GG"
          
          > 
  Guernsey  </option> 
  <option value="GN"
          
          > 
  Guinea  </option> 
  <option value="GW"
          
          > 
  Guinea-Bissau  </option> 
  <option value="GY"
          
          > 
  Guyana  </option> 
  <option value="HT"
          
          > 
  Haiti  </option> 
  <option value="HM"
          
          > 
  Heard Island  </option> 
  <option value="HN"
          
          > 
  Honduras  </option> 
  <option value="HK"
          
          > 
  Hong Kong  </option> 
  <option value="HU"
          
          > 
  Hungary  </option> 
  <option value="IS"
          
          > 
  Iceland  </option> 
  <option value="IN"
          
            selected 
          
          > 
  India  </option> 
  <option value="ID"
          
          > 
  Indonesia  </option> 
  <option value="IR"
          
          > 
  Iran  </option> 
  <option value="IQ"
          
          > 
  Iraq  </option> 
  <option value="IE"
          
          > 
  Ireland  </option> 
  <option value="IM"
          
          > 
  Isle of Man  </option> 
  <option value="IL"
          
          > 
  Israel  </option> 
  <option value="IT"
          
          > 
  Italy  </option> 
  <option value="JM"
          
          > 
  Jamaica  </option> 
  <option value="JP"
          
          > 
  Japan  </option> 
  <option value="JE"
          
          > 
  Jersey  </option> 
  <option value="JO"
          
          > 
  Jordan  </option> 
  <option value="KZ"
          
          > 
  Kazakhstan  </option> 
  <option value="KE"
          
          > 
  Kenya  </option> 
  <option value="KI"
          
          > 
  Kiribati  </option> 
  <option value="KW"
          
          > 
  Kuwait  </option> 
  <option value="KG"
          
          > 
  Kyrgyzstan  </option> 
  <option value="LA"
          
          > 
  Laos  </option> 
  <option value="LV"
          
          > 
  Latvia  </option> 
  <option value="LB"
          
          > 
  Lebanon  </option> 
  <option value="LS"
          
          > 
  Lesotho  </option> 
  <option value="LR"
          
          > 
  Liberia  </option> 
  <option value="LY"
          
          > 
  Libya  </option> 
  <option value="LI"
          
          > 
  Liechtenstein  </option> 
  <option value="LT"
          
          > 
  Lithuania  </option> 
  <option value="LU"
          
          > 
  Luxembourg  </option> 
  <option value="MO"
          
          > 
  Macao  </option> 
  <option value="MK"
          
          > 
  Macedonia  </option> 
  <option value="MG"
          
          > 
  Madagascar  </option> 
  <option value="MW"
          
          > 
  Malawi  </option> 
  <option value="MY"
          
          > 
  Malaysia  </option> 
  <option value="MV"
          
          > 
  Maldives  </option> 
  <option value="ML"
          
          > 
  Mali  </option> 
  <option value="MT"
          
          > 
  Malta  </option> 
  <option value="MH"
          
          > 
  Marshall Islands  </option> 
  <option value="MQ"
          
          > 
  Martinique  </option> 
  <option value="MR"
          
          > 
  Mauritania  </option> 
  <option value="MU"
          
          > 
  Mauritius  </option> 
  <option value="YT"
          
          > 
  Mayotte  </option> 
  <option value="MX"
          
          > 
  Mexico  </option> 
  <option value="FM"
          
          > 
  Micronesia  </option> 
  <option value="MD"
          
          > 
  Moldova  </option> 
  <option value="MC"
          
          > 
  Monaco  </option> 
  <option value="MN"
          
          > 
  Mongolia  </option> 
  <option value="ME"
          
          > 
  Montenegro  </option> 
  <option value="MS"
          
          > 
  Montserrat  </option> 
  <option value="MA"
          
          > 
  Morocco  </option> 
  <option value="MZ"
          
          > 
  Mozambique  </option> 
  <option value="MM"
          
          > 
  Myanmar  </option> 
  <option value="NA"
          
          > 
  Namibia  </option> 
  <option value="NR"
          
          > 
  Nauru  </option> 
  <option value="NP"
          
          > 
  Nepal  </option> 
  <option value="NL"
          
          > 
  Netherlands  </option> 
  <option value="AN"
          
          > 
  Netherlands Antilles  </option> 
  <option value="NC"
          
          > 
  New Caledonia  </option> 
  <option value="NZ"
          
          > 
  New Zealand  </option> 
  <option value="NI"
          
          > 
  Nicaragua  </option> 
  <option value="NE"
          
          > 
  Niger  </option> 
  <option value="NG"
          
          > 
  Nigeria  </option> 
  <option value="NU"
          
          > 
  Niue  </option> 
  <option value="NF"
          
          > 
  Norfolk Island  </option> 
  <option value="MP"
          
          > 
  Northern Mariana Islands  </option> 
  <option value="KP"
          
          > 
  North Korea  </option> 
  <option value="NO"
          
          > 
  Norwa  </option> 
  <option value="OM"
          
          > 
  Oman  </option> 
  <option value="PK"
          
          > 
  Pakistan  </option> 
  <option value="PW"
          
          > 
  Palau  </option> 
  <option value="PS"
          
          > 
  Palestinian Territories  </option> 
  <option value="PA"
          
          > 
  Panama   </option> 
  <option value="PG"
          
          > 
  Papua New Guinea  </option> 
  <option value="PY"
          
          > 
  Paraguay  </option> 
  <option value="PE"
          
          > 
  Peru  </option> 
  <option value="PH"
          
          > 
  Philippines  </option> 
  <option value="PN"
          
          > 
  Pitcairn  </option> 
  <option value="PL"
          
          > 
  Poland  </option> 
  <option value="PT"
          
          > 
  Portugal  </option> 
  <option value="PR"
          
          > 
  Puerto Rico  </option> 
  <option value="QA"
          
          > 
  Qatar  </option> 
  <option value="RE"
          
          > 
  Reunion  </option> 
  <option value="RO"
          
          > 
  Romania  </option> 
  <option value="RU"
          
          > 
  Russia  </option> 
  <option value="RW"
          
          > 
  Rwanda  </option> 
  <option value="SH"
          
          > 
  Saint Helena  </option> 
  <option value="KN"
          
          > 
  Saint Kitts and Nevis  </option> 
  <option value="LC"
          
          > 
  Saint Lucia  </option> 
  <option value="PM"
          
          > 
  Saint Pierre  </option> 
  <option value="VC"
          
          > 
  Saint Vincent  </option> 
  <option value="WS"
          
          > 
  Samoa  </option> 
  <option value="SM"
          
          > 
  San Marino  </option> 
  
  <option value="SA"
          
          > 
  Saudi Arabia  </option> 
  <option value="SN"
          
          > 
  Senegal  </option> 
  <option value="RS"
          
          > 
  Serbia  </option> 
  <option value="CS"
          
          > 
  Serbia and Montenegro  </option> 
  <option value="SC"
          
          > 
  Seychelles  </option> 
  <option value="SL"
          
          > 
  Sierra Leone  </option> 
  <option value="SG"
          
          > 
  Singapore  </option> 
  <option value="SK"
          
          > 
  Slovakia  </option> 
  <option value="SI"
          
          > 
  Slovenia  </option> 
  <option value="SB"
          
          > 
  Solomon Islands  </option> 
  <option value="SO"
          
          > 
  Somalia  </option> 
  <option value="ZA"
          
          > 
  South Africa  </option> 
  <option value="GS"
          
          > 
  South Georgia  </option> 
  <option value="KR"
          
          > 
  South Korea  </option> 
  <option value="ES"
          
          > 
  Spain  </option> 
  <option value="LK"
          
          > 
  Sri Lanka  </option> 
  <option value="SD"
          
          > 
  Sudan  </option> 
  <option value="SR"
          
          > 
  Suriname  </option> 
  <option value="SJ"
          
          > 
  Svalbard and Jan Mayen  </option> 
  <option value="SZ"
          
          > 
  Swaziland  </option> 
  <option value="SE"
          
          > 
  Sweden  </option> 
  <option value="CH"
          
          > 
  Switzerland  </option> 
  <option value="SY"
          
          > 
  Syria  </option> 
  <option value="TW"
          
          > 
  Taiwan  </option> 
  <option value="TJ"
          
          > 
  Tajikistan  </option> 
  <option value="TZ"
          
          > 
  Tanzania  </option> 
  <option value="TH"
          
          > 
  Thailand  </option> 
  <option value="TL"
          
          > 
  Timor-Leste  </option> 
  <option value="TG"
          
          > 
  Togo  </option> 
  <option value="TK"
          
          > 
  Tokelau  </option> 
  <option value="TO"
          
          > 
  Tonga  </option> 
  <option value="TT"
          
          > 
  Trinidad and Tobago  </option> 
  <option value="TN"
          
          > 
  Tunisia  </option> 
  <option value="TR"
          
          > 
  Turkey  </option> 
  <option value="TM"
          
          > 
  Turkmenistan  </option> 
  <option value="TC"
          
          > 
  Turks and Caicos  </option> 
  <option value="TV"
          
          > 
  Tuvalu  </option> 
  <option value="UG"
          
          > 
  Uganda  </option> 
  <option value="UA"
          
          > 
  Ukraine  </option> 
  <option value="AE"
          
          > 
  United Arab  </option> 
  <option value="GB"
          
          > 
  United Kingdom  </option> 
  <option value="US"
          
          > 
  United States  </option> 
  
  <option value="UY"
          
          > 
  Uruguay  </option> 
  <option value="UZ"
          
          > 
  Uzbekistan  </option> 
  <option value="VU"
          
          > 
  Vanuatu  </option> 
  <option value="VA"
          
          > 
  Vatican City  </option> 
  <option value="VE"
          
          > 
  Venezuela  </option> 
  <option value="VN"
          
          > 
  Vietnam  </option> 
  <option value="VG"
          
          > 
  Virgin Islands, British  </option> 
  <option value="VI"
          
          > 
  Virgin Islands, U.S.  </option> 
  <option value="WF"
          
          > 
  Wallis and Futuna  </option> 
  <option value="EH"
          
          > 
  Western Sahara  </option> 
  <option value="YE"
          
          > 
  Yemen  </option> 
  <option value="ZM"
          
          > 
  Zambia  </option> 
  <option value="ZW"
          
          > 
  Zimbabwe  </option> 
                          
                        </select></td>
                    </tr>
                    <tr>
                      <td align="left">* Region/State</td>
                      <td align="left"><input name="state" id="state" type="text" class="input_style" /></td>
                    </tr>
                  </table>
                  <br />
                  <table  style="border:1px #000066 dotted; padding-left:15px;" width="100%">
                    <tr>
                      <td width="180" align="left" style="widows:200px;">* Password</td>
                      <td width="358" align="left"><input name="password" id="password" type="password" class="input_style" /></td>
                    </tr>
                    <tr>
                      <td align="left">* Confirm password</td>
                      <td align="left"><input name="confirm_password" id="confirm_password" type="password" class="input_style" /></td>
                    </tr>
                  </table>
                  <br />
                  <div align="center">
                    <input type="submit" value="  Register  " />
                  </div>
                  <br />
                  <br />
                  <br /></td>
              </tr>
            </form>
          </table></td>
        <td width="200" valign="top"><br>
          <div >
          <div align="center" class="theme_color2" style="width:200px;">SHOPPING CART</div>
          <?php
  if(isset($_SESSION['customer_id']))
  {
$customer_id=$_SESSION['customer_id'];
$res=mysql_query("SELECT * FROM tb_shoppingcart where customer_id='$customer_id'");
echo '<table width="200px;"  style="border:#000099 1px dotted;">';
while($row=mysql_fetch_array($res))
	{
			echo '<tr><td align="left">'. $row['quantity'].'</td><td align="left"> x ';?>
          <a href=product.php?product_id=<?php echo $row['product_id'];?>><?php echo $row['product_name'].'</a></td><td align="left">'.$row['quantity']*$row['price'].'</td></tr>';
}
?>
          <tr>
              <td colspan="3"><div align="center"><a href="clear_cart.php">Clear my cart </a></div></td>
            </tr>
            <?php
}
else
{
echo '<div align="center" style="border:#000099 1px dotted; width:198px;">No Items</div>';
}
?>
      </tr>
    </table>
	<?php include('footer.php');?>
  </div>
</div>
</body>
</html>
